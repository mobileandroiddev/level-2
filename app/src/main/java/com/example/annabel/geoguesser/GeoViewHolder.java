package com.example.annabel.geoguesser;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

public class GeoViewHolder extends RecyclerView.ViewHolder {

    public ImageView image;
    public View view;

    public GeoViewHolder(View itemView) {
        super(itemView);
        image = itemView.findViewById(R.id.countryImageView);
        view = itemView;
    }
}
