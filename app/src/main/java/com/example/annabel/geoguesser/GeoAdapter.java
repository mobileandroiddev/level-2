package com.example.annabel.geoguesser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class GeoAdapter extends RecyclerView.Adapter<GeoViewHolder> {

    private Context context;
    public List<GeoLocations> geoLocations;

    public GeoAdapter(Context context, List<GeoLocations> geoList) {
        this.context = context;
        this.geoLocations = geoList;
    }

    @Override
    public GeoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view, parent, false);
        return new GeoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GeoViewHolder holder, final int position) {
        final GeoLocations geoLocation = geoLocations.get(position);
        holder.image.setImageResource(geoLocation.getImage());
    }

    @Override
    public int getItemCount() {
        return geoLocations.size();
    }


}
