package com.example.annabel.geoguesser;

public class GeoLocations {

    private boolean countryInEurope;
    private int countryNumber;
    public static int[] images = {R.drawable.img1_yes_denmark, R.drawable.img2_no_canada, R.drawable.img3_no_bangladesh, R.drawable.img4_yes_kazachstan, R.drawable.img5_no_colombia, R.drawable.img6_yes_poland, R.drawable.img7_yes_malta, R.drawable.img8_no_thailand};
    public static boolean[] countryInEuropeList = { true, false, false, true, false, true, true, false };

    public GeoLocations (boolean inEurope, int number){
     this.countryNumber = number;
     this.countryInEurope = inEurope;
    }

    public int getImage (){
        return countryNumber;
    }

    public boolean isEurope (){
        return countryInEurope;
    }
}
